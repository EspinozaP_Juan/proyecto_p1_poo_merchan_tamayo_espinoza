package Sistema;

import Aeronave.Aeronave;
import Aeronave.Helicoptero;
import Aeronave.Jet;
import Formato.Color;
import Persona.Perfil;
import Persona.Piloto;
import Persona.Usuario;
import Reservacion.Reserva;
import Reservacion.TicketPago;
import Reservacion.Tipo;
import Reservacion.Tour;
import Reservacion.Vuelo;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class TestReserva {

    public static ArrayList<Usuario> usuarios = new ArrayList<>();
    public static ArrayList<Piloto> pilotos = new ArrayList<>();
    public static ArrayList<Aeronave> aeronaves = new ArrayList<>();
    public static ArrayList<Vuelo> vuelos = new ArrayList<>();
    public static ArrayList<Tour> tours = new ArrayList<>();
    public static ArrayList<Reserva> reservas = new ArrayList<>();
    public static ArrayList<TicketPago> ticketsPago = new ArrayList<>();
    Usuario user = null;
    public static Scanner sc;

    public static void main(String[] args) {

        TestReserva test = new TestReserva();
        test.llenarArray("pilotos.txt");
        test.llenarArray("usuarios.txt");
        test.llenarArray("vuelos.txt");
        test.llenarArray("tours.txt");
        test.llenarArray("reservas.txt");
        test.llenarArray("pago.txt");
        //System.out.println(usuarios);
        //System.out.println(pilotos);
        //System.out.println(tours);
        //System.out.println(vuelos);
        //System.out.println(reservas);
        //System.out.println(ticketsPago);
        test.menu();

    }

    //==========================================================================
    /**
     * Metodo que permite llenar los arreglos con objetos pertenecientes al
     * sistema
     *
     * @param archivo nombre del archivo de texto qe contiene datos para crear
     * instancias Debe tener su extension
     */
    private void llenarArray(String archivo) {

        ArrayList<String> lineas = ManejoArchivo.leerArchivo(archivo);
        for (int i = 1; i < lineas.size(); i++) {
            String[] dato = lineas.get(i).split(";");
            StringTokenizer st = new StringTokenizer(lineas.get(i), ";");
            //System.out.println(dato);
            switch (archivo) {
                case "pilotos.txt":
                    while (st.hasMoreTokens()) {
                        Piloto p;
                        p = new Piloto(st.nextToken(), st.nextToken(),
                                st.nextToken(), st.nextToken(),
                                Integer.parseInt(st.nextToken()),
                                Integer.parseInt(st.nextToken()), st.nextToken());
                        pilotos.add(p);
                    }
                    break;

                case "usuarios.txt":
                    Usuario u;
                    Perfil perfil = null;
                    if (dato[6].equals("C")) {
                        perfil = Perfil.C;
                    } else if (dato[6].equals("O")) {
                        perfil = Perfil.O;
                    }
                    u = new Usuario(dato[0], dato[1], dato[2],
                            Integer.parseInt(dato[3]), dato[4], dato[5], perfil);
                    usuarios.add(u);
                    break;

                case "reservas.txt":
                    Reserva r;
                    Tipo tipo = null;
                    if (dato[5].equals("H")) {
                        tipo = Tipo.H;
                    } else if (dato[5].equals("J")) {
                        tipo = Tipo.J;
                    }
                    r = new Reserva(dato[1], dato[2], dato[3],
                            Integer.parseInt(dato[4]), tipo);
                    reservas.add(r);
                    break;
                case "pago.txt":
                    TicketPago t;
                    t = new TicketPago(dato[0], Integer.parseInt(dato[1]));
                    ticketsPago.add(t);
                    break;
                case "tours.txt":
                    Tour tr;
                    tr = new Tour(dato[0], dato[1], dato[2], dato[3]);
                    tours.add(tr);
                    break;

                case "vuelos.txt":
                    Vuelo v;
                    Tipo tipo2 = null;
                    Aeronave aeronave = null;
                    int capacidad = Integer.parseInt(dato[7]);
                    int capDisponible = Integer.parseInt(dato[8]);
                    int valorU = Integer.parseInt(dato[9]);
                    String codAeronave = dato[11];
                    if (dato[12].equals("H")) {
                        tipo2 = Tipo.H;
                        //codigo,capacidad
                        aeronave = new Helicoptero(codAeronave, capacidad);
                        aeronaves.add((Helicoptero) aeronave);
                    } else if (dato[12].equals("J")) {
                        tipo2 = Tipo.J;
                        aeronave = new Jet(codAeronave, capacidad);
                        aeronaves.add((Jet) aeronave);
                    }
                    String[] nombres = dato[10].split(" ");
                    Piloto piloto = null;
                    if (!pilotos.isEmpty()) {
                        for (Piloto p : pilotos) {
                            if (nombres[0].equals(p.getNombre())
                                    && nombres[1].equals(p.getApellido())) {
                                piloto = p;
                            }
                        }
                    }
                    //Conversion de String a LocalDate para fechas de partida y de llegada
                    DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate fechaPartida = null;
                    LocalDate fechaLlegada = null;
                    try {
                        fechaPartida = LocalDate.parse(dato[3], formato);
                        fechaLlegada = LocalDate.parse(dato[5], formato);

                    } catch (DateTimeParseException e) {
                        e.printStackTrace();
                    }
                    //Conversion de String a LocalTime para horas de salida y llegada
                    LocalTime horaPartida = LocalTime.parse(dato[4]);
                    LocalTime horaLlegada = LocalTime.parse(dato[6]);

                    v = new Vuelo(dato[0], dato[1], dato[2], fechaPartida,
                            horaPartida, fechaLlegada, horaLlegada, capacidad,
                            capDisponible, valorU, piloto, aeronave, tipo2);
                    vuelos.add(v);
                    break;
            }
        }
    }

    //==========================================================================
    /**
     * Metodo que permite muestra el menu inicial
     */
    private void menu() {

        sc = new Scanner(System.in);
        boolean salir = false;
        while (!salir) {
            System.out.println(
                    "\n================== SISTEMA DE RESERVA DE VUELOS PRIVADOS ==================\n");
            System.out.println("Para acceder al sistema, por favor"
                    + " ingrese su Usuario y Contraseña.\n");

            System.out.print("USUARIO>>>>    ");
            String usuario = sc.nextLine();
            System.out.print("CONTRASEÑA>>>> ");
            String contrasenia = sc.nextLine();
            System.out.println(">>Si desea salir ingrese la letra S seguida de Enter");
            System.out.print(">>Para ingresar presione la tecla Enter:  ");
            String salida = sc.nextLine().toUpperCase();
            if (salida.equals("S")) {
                salir = true;
                System.exit(0);
            } else if (!usuarios.isEmpty()) {
                for (Usuario u : usuarios) {
                    if (u.validarUsuario(usuario, contrasenia)) {
                        System.out.println(
                                "\n================== BIENVENIDO AL SISTEMA ==================\n");
                        System.out.println("Sr(a). " + u.getNombre() + " "
                                + u.getApellido());
                        user = u;
                        menuUsuario();
                    }
                }

                if (user == null) {
                    System.out.println("\nUSUARIO O CONTRASEÑA INCORRECTAS!");
                    System.out.println("====================================\n");
                    menu();
                }
            } else {
                System.out.println("NO EXISTEN USUARIOS REGISTRADOS!!!!");
                salir = true;
            }
        }
    }

    //==========================================================================
    /**
     * Metodo que permite muestra el menu del usuario segun sea cliente u
     * operador
     */
    private void menuUsuario() {
        sc = new Scanner(System.in);
        if (user.getPerfil().equals(Perfil.C)) {

            String opcion = "";
            while (!opcion.equals("6")) {
                System.out.println("\n============== MENU CLIENTE ==============\n");
                System.out.println("    1. Reservar Vuelo o Tour");
                System.out.println("    2. Consultar datos de piloto");
                System.out.println("    3. Consultar Vuelos privados");
                System.out.println("    4. Consultar Tours privados");
                System.out.println("    5. Consultar Paquetes Turisticos");
                System.out.println("    6. Salir");
                System.out.print("\n>>>Ingrese numero de opcion a realizar: ");

                opcion = sc.nextLine();
                switch (opcion) {
                    case "1":
                        menuReservar();
                        break;
                    case "2":
                        user.consultarPiloto();
                        break;
                    case "3":
                        user.consultarVuelos(Tipo.J);
                        break;
                    case "4":
                        user.consultarVuelos(Tipo.H);
                        break;
                    case "5":
                        user.consultarPaquetesTuristicos();
                        break;
                    case "6":
                        System.out.println("\n>>>>>>Gracias por su visita!!!!");
                        break;
                    default:
                        System.out.println(">>>INGRESE UNA OPCION VALIDA!!!\n");
                }
            }
            user.salir();
        } else if (user.getPerfil().equals(Perfil.O)) {
            String opcion = "";
            while (!opcion.equals("3")) {
                System.out.println("\n============ MENU OPERADOR ============\n");
                System.out.println("    1. Solicitar Combustible");
                System.out.println("    2. Cancelar");
                System.out.println("    3. Salir");
                System.out.print("\n>>>Ingrese numero de opcion"
                        + " que desee realizar: ");

                opcion = sc.nextLine();
                switch (opcion) {
                    case "1":
                        sc = new Scanner(System.in);
                        System.out.print("\n>>>Escoger codigo de aeronave\n");
                        for (Aeronave a : aeronaves) {
                            System.out.println("\t" + a.getCodigo());
                        }
                        System.out.print("\nIngrese el Codigo escogido: ");
                        String codigo = sc.nextLine();
                        String valor = "";
                        TreeSet<Aeronave> naves = new TreeSet<>(aeronaves);
                        for (Aeronave a : naves) {
                            if (a.getCodigo().equals(codigo)) {
                                valor = "+";
                            }
                        }
                        if (valor.equals("+")) {
                            user.solicitarCombustible(codigo);
                        }
                        break;
                    case "2":
                        String opc = "";
                        sc = new Scanner(System.in);
                        while (!opc.equals("3")) {
                            System.out.println("\n============ MENU CANCELAR ============\n");
                            System.out.println("    1. Cancelar Usuario");
                            System.out.println("    2. Cancelar Reserva");
                            System.out.println("    3. Atras");
                            System.out.print("\n>>>Ingrese numero de opcion"
                                    + " que desee realizar: ");

                            opc = sc.nextLine();
                            switch (opc) {
                                case "1":
                                    boolean b = false;
                                    while (!b) {
                                        try {
                                            System.out.println("");
                                            for (Usuario u : usuarios) {
                                                System.out.println(u);
                                            }

                                            System.out.print(">>>Ingrese cedula del ususario a Cancelar: ");
                                            String cedula = sc.nextLine();
                                            Usuario user1 = null;
                                            for (Usuario u : usuarios) {
                                                if (u.getCedula().equals(cedula)) {
                                                    user1 = u;
                                                    user.cancelar(user1);
                                                    b = true;
                                                }
                                            }
                                            if (user1.equals(null)) {
                                                System.out.println(">>>CEDULA INCORECTA!!!\n");
                                                throw new InputMismatchException();
                                            }
                                        } catch (InputMismatchException e) {
                                            sc.next();
                                            b = true;
                                        } catch (NullPointerException e) {
                                            System.out.print(">>>CEDULA INCORRECTA!!!\n");
                                            sc.next();
                                            b = true;
                                        }
                                    }
                                    break;
                                case "2":
                                    boolean b2 = false;
                                    while (!b2) {
                                        try {
                                            String patron = "%-11s %-15s %-15s %-13s %-12s %-8s%n";
                                            System.out.format(patron, "IdReserva", "Usuario", "FechaReserva",
                                                    "CodigoVuelo", "Pasajeros", "Tipo");
                                            for (Reserva re : reservas) {
                                                System.out.println(Arrays.asList(re.toString().split(";")));
                                            }

                                            System.out.print(">>>Ingrese idReserva de reserva a Cancelar: ");
                                            String id = sc.nextLine();
                                            Reserva reserva = null;
                                            for (Reserva r : reservas) {
                                                if (r.getIdReserva().equals(id)) {
                                                    reserva = r;
                                                    user.cancelar(reserva);
                                                    b2 = true;
                                                }
                                            }
                                            if (reserva.equals(null)) {
                                                System.out.println(">>>IDRESERVA INCORRECTO!!!");
                                                throw new InputMismatchException();
                                            }
                                        } catch (InputMismatchException e) {
                                            sc.next();
                                            b2 = true;

                                        } catch (NullPointerException e) {
                                            System.out.print(">>>IDRESERVA INCORRECTO!!!\n");
                                            sc.next();
                                            b2 = true;
                                        }
                                    }
                                    break;
                                case "3":
                                    break;
                                default:
                                    System.out.println(">>>INGRESE UNA OPCION VALIDA!!!\n");
                            }
                        }
                        break;
                    case "3":
                        System.out.println("\n>>>>>>Gracias por su visita!!!!");
                        break;
                    default:
                        System.out.println(">>>INGRESE UNA OPCION VALIDA!!!");
                }
            }
            user.salir();
        }
    }

    //==========================================================================
    /**
     * Metodo para el cliente que le muestra sus opciones del sistema
     *
     */
    private void menuReservar() {
        String opcion = "";
        while (!opcion.equals("3")) {
            System.out.println("\n=============== OPCION RESERVAR ===============\n");
            System.out.println("    1. Reservar Vuelo Privado");
            System.out.println("    2. Reservar Tour Privado");
            System.out.println("    3. Atras");
            System.out.print("\n>>>Ingrese numero de opcion que desee realizar: ");

            opcion = sc.nextLine();
            switch (opcion) {
                case "1":
                    reservarVuelo();
                    break;
                case "2":
                    reservarTour();
                    break;
                case "3":
                    menuUsuario();
                    break;
                default:
                    System.out.println(">>>INGRESE UNA OPCION VALIDA!!!\n");
            }
        }
    }

    //==========================================================================
       /**
     * Metodo para el cliente que le muestra sus opciones del sistema par reservar un vuelo privado
     *
     */

    public void reservarVuelo() {
        sc = new Scanner(System.in);
        TreeSet<String> origenes = new TreeSet<>();
        for (Vuelo v : vuelos) {
            origenes.add(v.getOrigen());
        }
        System.out.println("\n=============== RESERVAR VUELO ================\n");
        System.out.println("Selecciones uno de los siguientes datos enumerados:");
        System.out.println("\n-----Ciudades de Origen de Vuelo Disponibles------");
        int i = 1;
        for (String origen : origenes) {
            System.out.println("\t" + i + ". " + origen);
            i++;
        }
        int indice = 0;
        boolean bucle = false;
        System.out.print(">>>Ciudad de Origen: ");

        sc = new Scanner(System.in);
        while (!bucle) {
            try {
                indice = sc.nextInt();
                if (indice <= origenes.size()) {
                    bucle = true;
                } else {
                    throw new IndexOutOfBoundsException();
                }
            } catch (InputMismatchException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (i - 1) + ": ");
                sc.next();
            } catch (IndexOutOfBoundsException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (i - 1) + ": ");
            }
        }
        //Mostrar ciudades destino
        TreeSet<String> destinos = new TreeSet<>();
        String origen = new ArrayList<>(origenes).get(indice - 1);
        for (Vuelo v : vuelos) {
            if (origen.equals(v.getOrigen())) {
                destinos.add(v.getDestino());
            }
        }
        System.out.println("\n-----Ciudades de Destino de Vuelo Disponibles------");
        int j = 1;
        for (String destino : destinos) {
            System.out.println("\t" + j + ". " + destino);
            j++;
        }
        int ind = 0;
        boolean bucle2 = false;
        System.out.print(">>>Ciudad de Destino: ");
        sc = new Scanner(System.in);
        while (!bucle2) {
            try {
                ind = sc.nextInt();
                if (ind <= destinos.size()) {
                    bucle2 = true;
                } else {
                    throw new IndexOutOfBoundsException();
                }
            } catch (InputMismatchException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (j - 1) + ": ");
                sc.next();
            } catch (IndexOutOfBoundsException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (j - 1) + ": ");
            }
        }
        String destino = new ArrayList<>(destinos).get(ind - 1);

        TreeSet<LocalDate> fechas = new TreeSet<>();
        for (Vuelo v : TestReserva.vuelos) {
            if (v.getOrigen().equals(origen) && v.getDestino().equals(destino)) {
                fechas.add(v.getFechaPartida());
            }
        }
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("\n-----Fechas Disponibles de Salida de Vuelo Disponibles------");
        System.out.println("------------------Formato dd/mm/aaaa------------------------");
        //Muestro fechas que concuerdan con la las ciudades escogidas
        int ii = 1;
        for (LocalDate fecha : fechas) {
            System.out.println("\t" + ii + ". " + fecha.format(formato));
            ii++;
        }
        int ind2 = 0;
        boolean bucle3 = false;
        sc = new Scanner(System.in);
        System.out.print(">>>Fecha de Salida: ");
        while (!bucle3) {
            try {
                ind2 = sc.nextInt();
                if (ind2 <= fechas.size()) {
                    bucle3 = true;
                } else {
                    throw new IndexOutOfBoundsException();
                }
            } catch (InputMismatchException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (ii - 1) + ": ");
                sc.next();
            } catch (IndexOutOfBoundsException e) {
                System.out.print(">>>Debe ingresar un numero menor o igual a " + (ii - 1) + ": ");
            }
        }
        LocalDate fechaPartida = new ArrayList<>(fechas).get(ind2 - 1);

        //ingreso de numero de pasajeros               
        int nPasajeros = 0;
        int n = 0;
        while (n == 0) {
            try {
                System.out.print(">>>Numero de Pasajeros: ");
                nPasajeros = sc.nextInt();
                n = 1;
            } catch (RuntimeException e) {
                System.out.println(">>>DEBE INGRESAR UN NUMERO!!!");
                sc.next();
            }
        }
        ArrayList<Vuelo> vuelosJet = user.mostrarVuelos(origen, destino,
                fechaPartida, nPasajeros, Tipo.J);
        //user.mostrarVuelos(origen, destino, fechaPartida, nPasajeros);
        if (!vuelosJet.isEmpty()) {
            sc = new Scanner(System.in);
            boolean bucle4 = false;
            String codigo = "";
            while (!bucle4) {

                System.out.print("\n>>>Ingrese Codigo de vuelo que desea:");
                codigo = sc.nextLine();
                for (Vuelo v : vuelosJet) {
                    if (codigo.equals(v.getCodigo())) {
                        bucle4 = true;
                    } else {
                        codigo = "null";
                    }
                }
                if (codigo.equals("null")) {
                    System.out.println(">>>CODIGO ERRONEO!!!");
                }
            }
            System.out.println(">>>Desea reservar dicho vuelo?:");
            System.out.print(">>>Escriba la letra S para ACEPTAR o"
                    + " solo pulse la tecla ENTER para NEGAR:  ");
            String respuesta = sc.nextLine().toUpperCase();
            if (respuesta.equals("S")) {
                user.reservar(codigo, nPasajeros, Tipo.J);
                menuReservar();
            } else {
                System.out.println("\n>>>RESERVA CANCELADA!!!\n");
                menuReservar();
            }
        } else {
            System.out.println("\n>>>Numero de pasajeros NO DISPONIBLE!!\n");
        }
    }

    //==========================================================================
       /**
     * Metodo para el cliente que le muestra sus opciones del sistema para reservar un tour privado
     *
     */

    public void reservarTour() {
        sc = new Scanner(System.in);
        TreeSet<Tour> tours1 = new TreeSet<>(TestReserva.tours);

        boolean bucle = false;
        while (!bucle) {
            System.out.println("\n============== RESERVAR TOUR ==============\n");
            System.out.println(">>>Tours Disponibles:\n");

            int i = 1;
            for (Tour t : tours1) {
                System.out.println("\t" + i + ". " + t.getNombre());
                i++;
            }
            System.out.print("\n>>>Ingrese el numero del tour escogido: ");
            int indice = 0;
            boolean bucle1 = false;

            while (!bucle1) {
                try {
                    indice = sc.nextInt();
                    if (indice <= tours1.size()) {
                        bucle1 = true;
                    } else {
                        throw new IndexOutOfBoundsException();
                    }
                } catch (InputMismatchException e) {
                    System.out.print(">>>Debe ingresar un numero menor o igual a " + (i - 1) + ": ");
                    sc.next();
                } catch (IndexOutOfBoundsException e) {
                    System.out.print(">>>Debe ingresar un numero menor o igual a " + (i - 1) + ": ");
                }
            }
            Tour tourEscogido = new ArrayList<>(tours1).get(indice - 1);
            String nombreTour = tourEscogido.getNombre();

            System.out.println(Color.CYAN_BOLD + "\n================= " + nombreTour + " ==================\n"
                    + Color.RESET);
            String descripcion = tourEscogido.getDescripcion();
            if (descripcion.length() % 2 == 0) {
                int ind = descripcion.length() / 4;
                System.out.println(Color.BLUE_BOLD + descripcion.substring(0, ind));
                System.out.println(Color.BLUE_BOLD + descripcion.substring(ind, ind * 2));
                System.out.println(Color.BLUE_BOLD + descripcion.substring(ind * 2, ind * 3));
                System.out.println(Color.BLUE_BOLD + descripcion.substring(ind * 3, ind * 4)
                        + Color.RESET);
            } else {
                int ind = descripcion.length() / 3;
                System.out.println(Color.BLUE_BOLD + descripcion.substring(0, ind));
                System.out.println(Color.BLUE_BOLD + descripcion.substring(ind, ind * 2));
                System.out.println(Color.BLUE_BOLD + descripcion.substring(ind * 2, ind * 3)
                        + Color.RESET);
            }
            sc = new Scanner(System.in);
            System.out.println("\n>>>Desea reservar este Tour?: ");
            System.out.print(">>>Escriba la letra S para ACEPTAR o"
                    + " solo pulse la tecla ENTER para volver al MENU:  ");

            String respuesta1 = sc.nextLine().toUpperCase();

            ArrayList<Vuelo> vuelosTour = new ArrayList<>();

            if (respuesta1.equals("S")) {

                for (Tour tour : TestReserva.tours) {
                    if (tour.getNombre().equals(nombreTour)) {
                        for (Vuelo v : vuelos) {
                            if (v.getCodigo().equals(tour.getCodigoVuelo())) {
                                vuelosTour.add(v);
                            }
                        }
                    }
                }
                TreeSet<LocalDate> fechas = new TreeSet<>();

                for (Vuelo v : vuelosTour) {
                    fechas.add(v.getFechaPartida());
                }

                DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                System.out.println("\n============= Fechas Disponibles " + nombreTour + " ===============");
                System.out.println("------------------Formato dd/mm/aaaa------------------------");

                //Muestro fechas que concuerdan con la las ciudades escogidas
                int ii = 1;
                for (LocalDate fecha : fechas) {
                    System.out.println("\t" + ii + ". " + fecha.format(formato));
                    ii++;
                }
                int ind2 = 0;
                boolean bucle3 = false;
                sc = new Scanner(System.in);
                System.out.print(">>>Escoga Fecha de Salida: ");
                while (!bucle3) {
                    try {
                        ind2 = sc.nextInt();
                        if (ind2 <= fechas.size()) {
                            bucle3 = true;
                        } else {
                            throw new IndexOutOfBoundsException();
                        }
                    } catch (InputMismatchException e) {
                        System.out.print(">>>Debe ingresar un numero menor o igual a " + (ii - 1) + ": ");
                        sc.next();
                    } catch (IndexOutOfBoundsException e) {
                        System.out.print(">>>Debe ingresar un numero menor o igual a " + (ii - 1) + ": ");
                    }
                }

                LocalDate fechaPartida = new ArrayList<>(fechas).get(ind2 - 1);

                //ingreso de numero de pasajeros               
                int nPasajero = 0;
                int n = 0;
                while (n == 0) {
                    try {
                        System.out.print(">>>Ingrese Numero de Pasajeros: ");
                        nPasajero = sc.nextInt();
                        n = 1;
                    } catch (RuntimeException e) {
                        System.out.println(">>>DEBE INGRESAR UN NUMERO!!!");
                        sc.next();
                    }
                }

                String origen = vuelosTour.get(0).getOrigen();
                String destino = vuelosTour.get(0).getDestino();

                ArrayList<Vuelo> vuelosTourFinal = user.mostrarVuelos(origen, destino,
                        fechaPartida, nPasajero, Tipo.H);
                //user.mostrarVuelos(origen, destino, fechaPartida, nPasajeros);

                Collections.sort(vuelosTourFinal);
                if (!vuelosTourFinal.isEmpty()) {
                    sc = new Scanner(System.in);
                    boolean buclee = false;
                    String codigo1 = "";
                    while (!buclee) {
                        System.out.print("\n>>>Ingrese Codigo de vuelo que desea: ");
                        codigo1 = sc.nextLine();

                        for (Vuelo vv : vuelosTourFinal) {
                            if (vv.getCodigo().equalsIgnoreCase(codigo1)) {
                                buclee = true;
                            } else {
                                codigo1 = "none";
                            }
                        }
                        if (codigo1.equals("none")) {
                            System.out.println(">>>CODIGO ERRONEO!!!");
                        }
                    }
                    System.out.println("\n>>>Desea reservar dicho vuelo?:");
                    System.out.print(">>>Escriba la letra S para ACEPTAR o"
                            + " solo pulse la tecla ENTER para NEGAR:  ");

                    sc = new Scanner(System.in);
                    String respuesta2 = sc.nextLine().toUpperCase();
                    if (respuesta2.equals("S")) {
                        user.reservar(codigo1, nPasajero, Tipo.H);
                        menuReservar();
                    } else {
                        System.out.println("\n>>>RESERVA CANCELADA!!!\n");
                        menuReservar();
                    }
                } else {
                    System.out.println("\n>>>Numero de pasajeros NO DISPONIBLE!!\n");
                }
            } else {
                System.out.println("\n>>>RESERVA CANCELADA!!!\n");
            }
        }
    }
}
