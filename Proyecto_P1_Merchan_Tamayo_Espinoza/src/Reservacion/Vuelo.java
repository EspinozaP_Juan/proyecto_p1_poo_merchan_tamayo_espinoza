package Reservacion;

import Aeronave.Aeronave;
import Persona.Piloto;
import com.sun.javafx.binding.StringFormatter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Vuelo implements Comparable<Vuelo> {

    private String codigo; //Codigo de vuelo
    private String origen; //Ciudad Origen
    private String destino; //Ciudad Destino
    private LocalDate fechaPartida;
    private LocalTime horaPartida;
    private LocalDate fechaLlegada;
    private LocalTime horaLlegada;
    private int capacidad;
    private int capacidadDisponible;
    private int valorUnitario;
    private Piloto piloto;
    private Aeronave aeronave;
    private Tipo tipo;
    private DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Vuelo(String codigo, String origen, String destino,
            LocalDate fechaPartida, LocalTime horaPartida, LocalDate fechaLlegada,
            LocalTime horaLlegada, int capacidad, int capacidadDisponible,
            int valorUnitario, Piloto piloto, Aeronave aeronave, Tipo tipo) {
        this.codigo = codigo;
        this.origen = origen;
        this.destino = destino;
        this.fechaPartida = fechaPartida;
        this.horaPartida = horaPartida;
        this.fechaLlegada = fechaLlegada;
        this.horaLlegada = horaLlegada;
        this.capacidad = capacidad;
        this.capacidadDisponible = capacidadDisponible;
        this.valorUnitario = valorUnitario;
        this.piloto = piloto;
        this.aeronave = aeronave;
        this.tipo = tipo;
    }

    public Vuelo(String origen, String destino, LocalDate fechaPartida,
            int capacidadDisponible, Tipo tipo) {
        this.origen = origen;
        this.destino = destino;
        this.fechaPartida = fechaPartida;
        this.horaPartida = null;
        this.fechaLlegada = null;
        this.horaLlegada = null;
        this.capacidad = 0;
        this.capacidadDisponible = capacidadDisponible;
        this.valorUnitario = 0;
        this.piloto = null;
        this.aeronave = null;
        this.tipo = tipo;
    }

    //Metodo que permite modificar la direccion de memoria de cada instancia
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.origen);
        hash = 23 * hash + Objects.hashCode(this.destino);
        hash = 23 * hash + Objects.hashCode(this.fechaPartida);
        hash = 23 * hash + this.capacidadDisponible;
        hash = 23 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    //Metodo que permite comparar dos objetos segun atributos escogidos
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {//misma direccion de memoria
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vuelo other = (Vuelo) obj;
        if (this.capacidadDisponible <= other.capacidadDisponible) {
            return false;
        }
        if (!Objects.equals(this.origen, other.origen)) {
            return false;
        }
        if (!Objects.equals(this.destino, other.destino)) {
            return false;
        }
        if (!Objects.equals(this.fechaPartida, other.fechaPartida)) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Vuelo v) {
        int estado = this.fechaPartida.compareTo(v.fechaPartida);
        if (estado == 0) {
            estado = Integer.compare(this.valorUnitario, v.valorUnitario);
        }
        return estado;
    }

    //getters y setters
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public LocalDate getFechaPartida() {
        return fechaPartida;
    }

    public void setFechaPartida(LocalDate fechaPartida) {
        this.fechaPartida = fechaPartida;
    }

    public LocalTime getHoraPartida() {
        return horaPartida;
    }

    public void setHoraPartida(LocalTime horaPartida) {
        this.horaPartida = horaPartida;
    }

    public LocalDate getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(LocalDate fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    public LocalTime getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(LocalTime horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getCapacidadDisponible() {
        return capacidadDisponible;
    }

    public void setCapacidadDisponible(int capacidadDisponible) {
        this.capacidadDisponible = capacidadDisponible;
    }

    public int getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(int valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Piloto getPiloto() {
        return piloto;
    }

    public void setPiloto(Piloto piloto) {
        this.piloto = piloto;
    }

    public Aeronave getAeronave() {
        return aeronave;
    }

    public void setAeronave(Aeronave aeronave) {
        this.aeronave = aeronave;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    @Override
    public String toString() {
        String formato2 = "%-10s %-13s %-15s %-15s %-14s %-15s %-14s %-13s %-13s %-15s %-20s %-15s%n";
        String fechaP = fechaPartida.format(formato);
        String fechaL = fechaLlegada.format(formato);
        return String.format(formato2, codigo, origen, destino, fechaP,
                horaPartida, fechaL, horaLlegada, capacidad,
                capacidadDisponible, valorUnitario, piloto.getNombre() + " "
                + piloto.getApellido(), aeronave.getCodigo());
    }
}
