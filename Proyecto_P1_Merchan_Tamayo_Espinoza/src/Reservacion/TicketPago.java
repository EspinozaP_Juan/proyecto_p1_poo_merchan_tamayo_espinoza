package Reservacion;

import Sistema.TestReserva;
import java.io.Serializable;

public class TicketPago implements Serializable{

    private String idReserva;
    //valorPagar = precioUnitario*pasajeros
    private int valorPagar;

    //Constructores
    public TicketPago(Reserva reserva) {
        this.idReserva = reserva.getIdReserva();
        this.valorPagar = calcularPago(reserva);
    }

    public TicketPago(String idReserva, int valorPagar) {
        this.idReserva = idReserva;
        this.valorPagar = valorPagar;
    }

    private int calcularPago(Reserva reserva){
        String codigo = reserva.getCodigoVuelo();
        int pago = 0;
        for (Vuelo v : TestReserva.vuelos){
            if(codigo.equals(v.getCodigo())){
                pago = reserva.getPasajeros()*v.getValorUnitario();
            }
        }
        return pago;
    }
    
    //getters y setters
    public String getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(String idReserva) {
        this.idReserva = idReserva;
    }

    public int getValorPagar() {
        return valorPagar;
    }

    public void setValorPagar(int valorPagar) {
        this.valorPagar = valorPagar;
    }

    @Override
    public String toString() {
        return idReserva + ";" + valorPagar;
    }

}
