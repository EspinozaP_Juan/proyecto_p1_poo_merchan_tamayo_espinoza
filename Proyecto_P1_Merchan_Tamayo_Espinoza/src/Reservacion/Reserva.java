package Reservacion;

import Persona.Cancelable;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Reserva implements Cancelable{
    
    private String idReserva;
    private String usuario;
    private String fechaReserva;
    private String codigoVuelo;
    private int pasajeros;
    private Tipo tipo;
    private static int contador = 0; 
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    
    //Constructor

    public Reserva(String usuario, String codigoVuelo, int pasajeros, Tipo tipo) {
        this.idReserva = generarID();
        this.usuario = usuario;
        this.fechaReserva = LocalDate.now().format(formatter);
        this.codigoVuelo = codigoVuelo;
        this.pasajeros = pasajeros;
        this.tipo = tipo;
    }

    public Reserva(String usuario, String fechaReserva, String codigoVuelo,
            int pasajeros, Tipo tipo) {
        this.idReserva = generarID();
        this.usuario = usuario;
        this.fechaReserva = fechaReserva;
        this.codigoVuelo = codigoVuelo;
        this.pasajeros = pasajeros;
        this.tipo = tipo;
    }

    //Metodo genera un ID consecutivo de 4 digitos a medida que se instancia
    private String generarID(){
        Reserva.contador++;
        String id = String.format("%04d", contador);
        return id;
        
    }

    //getters y setters
    public String getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(String idReserva) {
        this.idReserva = idReserva;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return idReserva + ";" + usuario + ";" + fechaReserva + ";" 
                + codigoVuelo + ";" + pasajeros + ";" + tipo;
    }

    @Override
    public void cancelar(Cancelable obj) {
    }
}
