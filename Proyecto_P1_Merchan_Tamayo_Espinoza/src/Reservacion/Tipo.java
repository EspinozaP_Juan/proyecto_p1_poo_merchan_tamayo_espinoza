package Reservacion;

import Persona.Perfil;

public enum Tipo {  
    
    //J, H;
    
    
    J("J"),H("H");
    private String tipo;

    private Tipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

   
    
}
