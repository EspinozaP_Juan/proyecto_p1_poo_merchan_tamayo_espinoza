
package Reservacion;

public class Tour implements Comparable<Tour>{

    private String idTour;
    private String codigoVuelo;
    private String nombre;
    private String descripcion;

    //constructor
    public Tour(String idTour, String codigoVuelo,
            String nombre, String descripcion) {
        this.idTour = idTour;
        this.codigoVuelo = codigoVuelo;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    //getters y setters
    public String getIdTour() {
        return idTour;
    }

    public void setIdTour(String idTour) {
        this.idTour = idTour;
    }

    public String getCodigoVuelo() {
        return codigoVuelo;
    }

    public void setCodigoVuelo(String codigoVuelo) {
        this.codigoVuelo = codigoVuelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Tour{" + "idTour=" + idTour + ", codigoVuelo=" + codigoVuelo 
                + ", nombre=" + nombre + ", descripcion=" + descripcion + "}\n";
    }

    @Override
    public int compareTo(Tour t) {
    int estado = this.nombre.compareTo(t.nombre);

        return estado;
    }
}
