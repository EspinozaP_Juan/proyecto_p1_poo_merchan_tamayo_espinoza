package Persona;

public class Piloto extends Persona {
    private String id;
    private int aniosExp;
    private String licencia;

    //Constructores
    public Piloto(String id, String cedula, String nombre, String apellido,
            int edad, int aniosExp, String licencia) {
        super(cedula, nombre, apellido, edad);
        this.id = id;
        this.aniosExp = aniosExp;
        this.licencia = licencia;
    }
    
    //getters y setters
    public int getAniosExp() {
        return aniosExp;
    }

    public void setAniosExp(int aniosExp) {
        this.aniosExp = aniosExp;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString() + " Piloto{" + "id= "+id +", aniosExperiencia= " 
                + aniosExp + ", licencia= " + licencia + '}'+"\n";
    }
}
