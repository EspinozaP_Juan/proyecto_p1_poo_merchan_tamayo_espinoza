
package Persona;

public interface Cancelable {
    
    void cancelar(Cancelable obj);
    
}
