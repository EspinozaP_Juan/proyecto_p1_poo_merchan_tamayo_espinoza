package Persona;

import Reservacion.Reserva;
import Reservacion.TicketPago;
import Reservacion.Tipo;
import Reservacion.Vuelo;
import Sistema.ManejoArchivo;
import Sistema.TestReserva;
import java.util.Scanner;
import Aeronave.*;
import java.util.ArrayList;
import Formato.Color;
import Reservacion.Tour;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

public class Usuario extends Persona implements Cancelable {

    private String correo;
    private String usuario;
    private String contrasenia;
    private Perfil perfil;

    //Constructores
    public Usuario(String cedula, String nombre, String apellido, int edad,
            String correo, String usuario, String contrasenia, Perfil perfil) {
        super(cedula, nombre, apellido, edad);
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.perfil = perfil;
    }

    public Usuario(String cedula, String nombre, String apellido, int edad,
            String usuario, String contrasenia, Perfil perfil) {
        super(cedula, nombre, apellido, edad);
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.perfil = perfil;
        this.correo = "";
    }

    //==========================================================================
    /**
     * Este Metodo que verifica si el usuario y contrania ingresados pertecen a
     * un Usuario registrado en los archivos de texto
     *
     * @param usuario Nombre de usuario
     * @param contrasenia Contrasenia de usuario
     * @return Boolean que comprueba si esta registra en el sistema
     *
     *
     */
    public boolean validarUsuario(String usuario, String contrasenia) {
        return (this.usuario.equals(usuario)
                && this.contrasenia.equals(contrasenia));
    }

    //==========================================================================
    /**
     * Este Metodo que permite consultar los datos del piloto establecido para
     * la reserva de la cual se ingrese su ID
     *
     *
     */
    public void consultarPiloto() {

        if (!TestReserva.reservas.isEmpty()) {
            Scanner sc = new Scanner(System.in);
            System.out.println("\n============= LISTA DE RESERVAS =============\n");

            String patron = "%-11s %-15s %-15s %-13s %-12s %-8s%n";
            System.out.format(patron, "IdReserva", "Usuario", "FechaReserva",
                    "CodigoVuelo", "Pasajeros", "Tipo");
            for (Reserva r : TestReserva.reservas) {
                String[] datos = r.toString().split(";");
                //String str = String.join("\t" + "\t", datos);
                System.out.format(patron, datos);
            }
            System.out.print("\n>>>Ingrese Id de Reserva"
                    + " de la cual desea consultar su piloto: ");
            String idReserva = sc.nextLine().toUpperCase();
            Reserva reserva = null;
            for (Reserva r : TestReserva.reservas) {
                if (r.getIdReserva().equals(idReserva)) {
                    reserva = r;
                    for (Vuelo v : TestReserva.vuelos) {
                        if (reserva.getCodigoVuelo().equals(v.getCodigo())) {
                            System.out.println("\n>>>Piloto asignado a esta Reserva");
                            System.out.println(v.getPiloto());
                        }
                    }
                }
            }
            if (reserva == null) {
                System.out.println("\n>>>>>IdReserva INCORRECTO!!!!!\n");
            }
        } else {
            System.out.println(">>>USTED NO CONSTA DE RESERVAS REALIZADAS!\n");
        }
    }

    //==========================================================================
    //
    /**
     * Metodo que permite terminar la ejecucion del programa para el usuario
     *
     *
     */
    public void salir() {
        System.exit(0);
    }

    //==========================================================================
    //
    /**
     * Metodo que muestra los vuelos disponibles segun los datos pedidos
     *
     * @param origen Ciudad de origen
     * @param destino Ciudad de destino
     * @param fechaPartida Fecha de Salida del vuelo
     * @param nPasajeros numero de pasajeros a viajar
     * @param tipo tipo de vuelo H (Helicoptero) o J (Jet)
     * @return Arreglo con los vuelos que constan con los datos ingresados
     *
     */
    public ArrayList<Vuelo> mostrarVuelos(String origen, String destino,
            LocalDate fechaPartida, int nPasajeros, Tipo tipo) {
        ArrayList<Vuelo> vuelos1 = new ArrayList<>();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Vuelo vuelo = new Vuelo(origen, destino, fechaPartida,
                nPasajeros, tipo);
        System.out.println("Ciudad Origen: " + origen);
        System.out.println("Ciudad Destino: " + destino);
        System.out.println("Fecha: " + fechaPartida.format(formato));
        System.out.println("Numero Pasajeros: " + nPasajeros + "\n");
        System.out.println("Vuelos Disponibles:\n");

        String patron = "%-10s %-13s %-15s %-15s %-14s %-15s %-14s %-13s %-13s %-15s %-20s %-15s%n";
        System.out.format(patron, "Codigo", "Origen", "Destino", "FechaPartida",
                "HoraPartida", "FechaLlegada", "HoraLlegada", "Capacidad",
                "Disponible", "ValorUnitario", "Piloto", "Aeronave");
        System.out.println("");
        Collections.sort(TestReserva.vuelos);
        for (Vuelo v : TestReserva.vuelos) {
            if (v.equals(vuelo)) {
                vuelos1.add(v);
                System.out.print(v);
            }
        }
        Collections.sort(vuelos1);
        return vuelos1;
    }

    //==========================================================================
    /**
     * Metodo reserva vuelo y genera ticket de pago, crea respectivos objetos
     *
     * @param codigoVuelo Codigo unico de vuelo
     * @param nPasajeros numero de pasajeros a viajar
     * @param tipo tipo de vuelo H (Helicoptero) o J (Jet)
     *
     */
    public void reservar(String codigoVuelo, int nPasajeros, Tipo tipo) {

        Reserva reserva;
        reserva = new Reserva(this.usuario, codigoVuelo, nPasajeros, tipo);
        TestReserva.reservas.add(reserva);
        ManejoArchivo.escribirArchivo("reservas.txt", reserva.toString());

        TicketPago ticket = new TicketPago(reserva);
        TestReserva.ticketsPago.add(ticket);
        ManejoArchivo.escribirArchivo("pago.txt", ticket.toString());

        //Disminuye la capacidad disponible cada vez que se hacen reservas
        for (Vuelo v : TestReserva.vuelos) {
            if (codigoVuelo.equals(v.getCodigo())) {
                int capacidad = v.getCapacidadDisponible() - nPasajeros;
                v.setCapacidadDisponible(capacidad);
            }
        }
        System.out.println(Color.BLUE_BOLD + "RESERVA REALIZADA CON EXITO!!!");
        System.out.println(Color.BLUE_BOLD + "TICKET DE PAGO GENERADO!!!");
        System.out.println(Color.BLUE_BOLD + "\n=========== TICKET DE PAGO GENERADO ===========\n");
        System.out.println(Color.BLUE_BOLD + "Nombre: " + this.nombre + " " + this.apellido);
        System.out.println(Color.BLUE_BOLD + "Vuelo: " + codigoVuelo);
        System.out.println(Color.BLUE_BOLD + "Valor a Pagar: $" + ticket.getValorPagar() + "\n");
        System.out.println(Color.BLUE_BOLD + "** Debe realizar el pago antes de 48 horas para "
                + "que su reserva sea válida.");
        System.out.println(Color.RESET);
    }

    //==========================================================================
    /**
     * Metodo que muestra los vuelos segun su aeronave
     *
     * @param tipo tipo de vuelo H (Helicoptero) o J (Jet)
     *
     */
    public void consultarVuelos(Tipo tipo) {

        ArrayList<Vuelo> vuelosPrivados = new ArrayList<>();
        for (Vuelo v : TestReserva.vuelos) {
            if (v.getTipo().equals(tipo)) {
                vuelosPrivados.add(v);
            }
        }
        Collections.sort(vuelosPrivados);
        System.out.println("");
        String patron = "%-10s %-13s %-15s %-15s %-14s %-15s %-14s %-13s %-13s %-15s %-20s %-15s%n";
        System.out.format(patron, "Codigo", "Origen", "Destino", "FechaPartida",
                "HoraPartida", "FechaLlegada", "HoraLlegada", "Capacidad",
                "Disponible", "ValorUnitario", "Piloto", "Aeronave");
        System.out.println("");
        for (Vuelo v : vuelosPrivados) {
            System.out.print(v);
        }
    }

    //==========================================================================
    /**
     * Metodo que muestre el nombre del paquete y su descripcion en consola
     *
     */
    public void consultarPaquetesTuristicos() {

        ArrayList<String> listaNombre = new ArrayList<String>();
        System.out.println("\n" + Color.RED_BOLD
                + "================== PAQUETES TURÍSTICOS ==================\n"
                + Color.RESET);
        for (Tour t : TestReserva.tours) {
            String nombre = t.getNombre();
            String descripcion = t.getDescripcion();
            if (!listaNombre.contains(nombre)) {
                listaNombre.add(nombre);
                System.out.println("\n" + Color.MAGENTA_BOLD + nombre + Color.RESET + "\n");
                if (!listaNombre.contains(descripcion)) {
                    listaNombre.add(descripcion);
                    if (descripcion.length() % 2 == 0) {
                        //System.out.println(Color.BLUE_BOLD + descripcion + Color.RESET);
                        int indice = descripcion.length() / 4;
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(0, indice));
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(indice, indice * 2));
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(indice * 2, indice * 3));
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(indice * 3, indice * 4)
                                + Color.RESET);
                    } else {
                        int indice = descripcion.length() / 3;
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(0, indice));
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(indice, indice * 2));
                        System.out.println(Color.BLUE_BOLD + descripcion.substring(indice * 2, indice * 3)
                                + Color.RESET);
                    }
                }
            }
        }

    }

    //==========================================================================
    @Override
    /**
     * Metodo exclusivo de un usuario operador Permite eliminar usuarios del
     * sistema y reservas
     *
     * @param obj Objeto de Referencia Cancelable (interface) la cual es
     * implementa por clase Usuario y Reserva
     */
    public void cancelar(Cancelable obj) {
        if (obj instanceof Usuario) {
            System.out.println(Color.BLUE_BOLD);
            System.out.println("Estimado operador, se procederá a "
                    + "desactivar el usuario del cliente "
                    + Color.BLUE_UNDERLINED + nombre + " " + apellido
                    + Color.RESET + Color.BLUE_BOLD
                    + ", en las próximas 48 horas, desde este momento ya no"
                    + " puede hacer transacciones de reserva en el sistema. ");
            System.out.println(Color.RESET);
        } else if (obj instanceof Reserva) {
            System.out.println(Color.BLUE_BOLD);
            System.out.println("Estimado operador, la reserva del cliente "
                    + Color.BLUE_UNDERLINED + nombre + " " + apellido
                    + Color.RESET + Color.BLUE_BOLD + " , con código"
                    + ((Reserva) obj).getCodigoVuelo() + "ha sido cancelada. ");
            System.out.println(Color.RESET);
        } else {
            System.out.println("Información ingresada incorrecta");
        }
    }

    //==========================================================================
    /**
     * Metodo exclusivo de un usuario operador Permite solicita recarga de
     * combustible para una aeronave segun su codigo
     *
     * @param codigoAeronave Codigo unico de aeronave
     */
    public void solicitarCombustible(String codigoAeronave) {
        if (this.perfil.equals(Perfil.O)) {
            for (Aeronave a : TestReserva.aeronaves) {
                if (a.getCodigo().equals(codigoAeronave)) {
                    if (a instanceof Helicoptero) {
                        ((Helicoptero) a).recargarGasolina();
                    } else if (a instanceof Jet) {
                        ((Jet) a).recargarGasolina();
                    }
                }
            }
        } else {
            System.out.println("Este usuario no tiene permitido "
                    + "realizar la siguiente acción");
        }
    }

    //==========================================================================
    //getters y setters
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    //==========================================================================
    @Override
    public String toString() {
        return super.toString() + " Usuario{" + "correo= " + correo
                + ", usuario= " + usuario + ", contrasenia= " + contrasenia
                + ", perfil= " + perfil + '}' + "\n";
    }
}
