
package Aeronave;

public abstract class Aeronave implements Recargable, Comparable<Aeronave>{
    protected String codigo ;
    protected String modelo;
    protected int capacidad;
    protected String aerolinea;

    //Constructores
    public Aeronave(String codigo, String modelo, 
            int capacidad, String aerolinea) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.aerolinea = aerolinea;
    }

    public Aeronave(String codigo, int capacidad) {
        this.codigo = codigo;
        this.modelo = "";
        this.capacidad = capacidad;
        this.aerolinea = "";
    }

    
    //getters y setters
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String getAerolinea() {
        return aerolinea;
    }

    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    @Override
    public int compareTo(Aeronave a){
        int estado = this.codigo.compareToIgnoreCase(a.getCodigo());
        return estado;
    }
    
    
    
    @Override
    public String toString() {
        return "Aeronave{" + "codigo=" + codigo + ", modelo=" + modelo + 
                ", capacidad=" + capacidad + ", aerolinea=" + aerolinea + '}';
    }
}
