package Aeronave;

import Formato.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Helicoptero extends Aeronave {

    private Rotor tipoRotor;
    private String codigoRotor;

    //Constructores
    public Helicoptero(String codigo, String modelo, int capacidad,
            String aerolinea, Rotor tipoRotor, String codigoRotor) {
        super(codigo, modelo, capacidad, aerolinea);
        this.tipoRotor = tipoRotor;
        this.codigoRotor = codigoRotor;
    }

    public Helicoptero(String codigo, int capacidad) {
        super(codigo, capacidad);
        this.tipoRotor = null;
        this.codigoRotor = "";
    }

    //Sobreescritura de metodo de interface Recargable
    /**
     * Metodo sobreescrito que permite generar una orden de abastecimeinto de
     * combustible
     */
    @Override
    public void recargarGasolina() {
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String fecha = LocalDate.now().format(formato);
        System.out.println(Color.BLUE_BOLD);
        System.out.println("El helicoptero con codigo " + codigo + " se genera una orden "
                + "de abastecimiento de combustible para la fecha " + fecha);
        System.out.println(Color.RESET);
    }

    //getters y setters
    public Rotor getTipoRotor() {
        return tipoRotor;
    }

    public void setTipoRotor(Rotor tipoRotor) {
        this.tipoRotor = tipoRotor;
    }

    public String getCodigoRotor() {
        return codigoRotor;
    }

    public void setCodigoRotor(String codigoRotor) {
        this.codigoRotor = codigoRotor;
    }

    @Override
    public String toString() {
        return super.toString() + " Helicoptero{" + "tipoRotor=" + tipoRotor
                + ", codigoRotor=" + codigoRotor + '}';
    }

}
