package Aeronave;

import Formato.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Jet extends Aeronave {

    //Tiempo de autonomia de vuelo en horas
    private int tiempoVuelo;

    //Constructores
    public Jet(String codigo, String modelo, int capacidad, String aerolinea, int tiempoVuelo) {
        super(codigo, modelo, capacidad, aerolinea);
        this.tiempoVuelo = tiempoVuelo;
    }

    public Jet(String codigo, int capacidad) {
        super(codigo, capacidad);
        this.tiempoVuelo = 0;
    }

    //Sobreescritura de metodo de interface Recargable
    /**
     * Metodo sobreescrito que permite generar una orden de abastecimeinto de
     * combustible
     */
    @Override
    public void recargarGasolina() {
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String fecha = LocalDate.now().plusDays(2).format(formato);
        System.out.println(Color.BLUE_BOLD);
        System.out.println("Para el jet con codigo " + codigo + " se genera una orden "
                + "de abastecimiento de combustible válida hasta la fecha "
                + fecha);
        System.out.println(Color.RESET);
    }

    //getters y setters
    public int getTiempoVuelo() {
        return tiempoVuelo;
    }

    public void setTiempoVuelo(int tiempoVuelo) {
        this.tiempoVuelo = tiempoVuelo;
    }

    @Override
    public String toString() {
        return super.toString() + " Jet{" + "tiempoVuelo=" + tiempoVuelo + '}';
    }

}
