package Aeronave;

public interface Recargable {

    /**
     * Metodo que permite generar una orden de abastecimeinto de combustible
     * para las aeronaves
     */
    void recargarGasolina();
}
